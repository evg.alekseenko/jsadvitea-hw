/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>

*/

var defaultComment = {
  avatarUrl: 'http://brk-gidroservice.com.ua/wp-content/uploads/2018/04/avatar-placeholder.png',
  addLike: function(){
    this.likes++;
  }
}
function Comment(name, text, avatarUrl){
  this.name = name;
  this.text = text;
  if( avatarUrl !== undefined){
    this.avatarUrl = avatarUrl;
  }

  this.likes = 0
  //this.__proto__ = defaultComment
  Object.setPrototypeOf(this, defaultComment);
}


var comment1 = new Comment('комментарий 1', 'текст комментария 1', 'https://media.forgecdn.net/avatars/124/768/636424778749237239.jpeg');
comment1.addLike();
comment1.addLike();
comment1.addLike();
var comment2 = new Comment('комментарий 2', 'текст комментария 2', 'https://cdn3.iconfinder.com/data/icons/avatars-15/64/_Ninja-2-512.png');
var comment3 = new Comment('комментарий 3', 'текст комментария 3', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR6zgx1awTl-MK4oeW2qYSdTvzOVp2qJrjb9XC6KGpKuG30wxtX');
var comment4 = new Comment('комментарий 4', 'текст комментария 4');
comment4.addLike();

var avaArray = [comment1, comment2, comment3, comment4];

function AddComments(array) {
  this.array = array,
  this.display = function() {
  var CommentsFeed = document.getElementById('CommentsFeed');
  array.forEach(element => {
    console.log(element);
    var commentBlock = document.createElement('div');
    commentBlock.style.clear='both';
    commentBlock.style.margin='5px';
    commentBlock.style.border='1px solid #ccc';
    commentBlock.style.position ='relative';

    var img = new Image(100,100);
    img.style.cssFloat = 'left';
    img.src = element.avatarUrl;// !=undefined ? element.avatarUrl : element.defaultAvatarUrl;
    commentBlock.append(img);

    var detailBlock = document.createElement('div');
    detailBlock.style.minHeight='100px';
    detailBlock.style.paddingLeft='0 110px';
    var strong = document.createElement('strong');
    strong.innerText = element.name;
    detailBlock.append(strong);
    var textBlock = document.createElement('div');
    textBlock.innerText = element.text;
    detailBlock.append(textBlock);

    var likeBlock = document.createElement('div');
    likeBlock.innerText = 'Likes: ' + element.likes;
    likeBlock.style.position='absolute';
    likeBlock.style.top = '10px';
    likeBlock.style.right = '10px';
    detailBlock.append(likeBlock);

    commentBlock.append(detailBlock);

    CommentsFeed.append(commentBlock);
  });
}
}

var addComments = new AddComments(avaArray);
addComments.display();