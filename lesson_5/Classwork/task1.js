/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/

var Train = {
    name: 'train name',
    speed: 0,
    passengersCount: 0,
    go() {
        this.speed = 100;
        // console.log(`Поезд ${this.name} везет $this.passengersCount со скоростью $this.speed`);
        console.log('Поезд ' + this.name +' везет '+ this.passengersCount +' со скоростью ' +this.speed);
    },
    stop() {
        this.speed = 0;
        // console.log(`Поезд $this.name остановился. Скорость $this.speed`);
        console.log('Поезд ' +this.name+' остановился. Скорость '+ this.speed);
    },
    takePassenger(count) {
        this.passengersCount += count;
    }
};

console.log(Train);
Train.go();
Train.stop();
Train.takePassenger(5);
Train.takePassenger(10);
Train.go();
Train.stop();
