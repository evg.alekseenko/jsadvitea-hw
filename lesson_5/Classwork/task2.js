/*

    Задание 2.

    Напишите фунцию, которая изменяет цвет-фона и цвет текста, присваивая к новым цветам
    значение из this.color, this.background
    А так же добавляет элемент h1 с текстом "I know how binding works in JS"

    1.1 Ф-я принимает один аргумент,
    второй попадает в него через метод .call(obj)

    1.2 Ф-я не принимает никаких аргументов,
    а необходимые настройки полностью передаются через bind

    1.3 Ф-я принимает фразу для заголовка,
    обьект с настройками передаем через .apply();

*/
  let colors = {
    background: 'purple',
    color: 'white'
  }

  function myCall1( back ){
    document.body.style.background = back;
    document.body.style.color = this.color;
  }
  myCall1.call( colors, 'red' );

  function myCall2(phrase)
  {
    var h1 = document.createElement('h1');
    h1.innerText = phrase;
    var conteiner = document.getElementById('conteiner2');
    conteiner.append(h1);
    document.body.style.background = this.background;
    document.body.style.color = this.color;
  }
  var callMyCall2 = myCall2.bind(colors, 'some phrase');
  callMyCall2();

  function myCall3(text1, text2, text3) {
    var h1 = document.createElement('h1');
    h1.innerText = text1 +' ' + text2 + ' '+ text3;
    var conteiner = document.getElementById('conteiner2');
    conteiner.append(h1);
    document.body.style.background = this.background;
    document.body.style.color = this.color;
  }
  myCall3.apply(colors, ['some', 'new', 'phrase']);