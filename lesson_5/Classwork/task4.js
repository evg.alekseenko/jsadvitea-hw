/*

  Задание "Шифр цезаря":

    https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F

    Написать функцию, которая будет принимать в себя слово и количество
    симовлов на которые нужно сделать сдвиг внутри.

    Написать функцию дешефратор которая вернет слово в изначальный вид.

    Сделать статичные функции используя bind и метод частичного
    вызова функции (каррирования), которая будет создавать и дешефровать
    слова с статично забитым шагом от одного до 5.


    Например:
      encryptCesar('Word', 3);
      encryptCesar1(...)
      ...
      encryptCesar5(...)

      decryptCesar1('Sdwq', 3);
      decryptCesar1(...)
      ...
      decryptCesar5(...)


      "а".charCodeAt(); // 1072
      String.fromCharCode(189, 43, 190, 61) // ½+¾

*/

function encryptCesar(step, text) {
  var array = [];
  for (var i=0;i<text.length;i++) {
    array[i] = text[i].charCodeAt()+step;  
  }
  return String.fromCharCode(...array);
}
function decryptCesar(step, text) {
  return encryptCesar(-step, text);
}

var encryptCesar1 = encryptCesar.bind(null, 1);
var encryptCesar2 = encryptCesar.bind(null, 2);
var encryptCesar3 = encryptCesar.bind(null, 3);
var encryptCesar4 = encryptCesar.bind(null, 4);
var encryptCesar5 = encryptCesar.bind(null, 5);

var decryptCesar1 = decryptCesar.bind(null, 1);
var decryptCesar2 = decryptCesar.bind(null, 2);
var decryptCesar3 = decryptCesar.bind(null, 3);
var decryptCesar4 = decryptCesar.bind(null, 4);
var decryptCesar5 = decryptCesar.bind(null, 5);


var txt = 'Hello from the other side'
console.log(txt);


var enc = encryptCesar1(txt)
console.log(enc);
var dec = decryptCesar1(enc);
console.log(dec);

var enc = encryptCesar4(txt)
console.log(enc);
var dec = decryptCesar4(enc);
console.log(dec);