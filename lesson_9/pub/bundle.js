/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/classes/Han.js":
/*!****************************!*\
  !*** ./src/classes/Han.js ***!
  \****************************/
/*! exports provided: Han */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Han\", function() { return Han; });\n/* harmony import */ var _functions_layEggs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../functions/layEggs */ \"./src/functions/layEggs.js\");\n/* harmony import */ var _functions_eat__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../functions/eat */ \"./src/functions/eat.js\");\n/* harmony import */ var _functions_run__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../functions/run */ \"./src/functions/run.js\");\n\r\n\r\n\r\nclass Han{\r\n    constructor(){\r\n        console.log('Han can:');\r\n        Object(_functions_layEggs__WEBPACK_IMPORTED_MODULE_0__[\"layEggs\"])();\r\n        Object(_functions_eat__WEBPACK_IMPORTED_MODULE_1__[\"eat\"])();\r\n        Object(_functions_run__WEBPACK_IMPORTED_MODULE_2__[\"run\"])();\r\n    }\r\n}\n\n//# sourceURL=webpack:///./src/classes/Han.js?");

/***/ }),

/***/ "./src/classes/Owl.js":
/*!****************************!*\
  !*** ./src/classes/Owl.js ***!
  \****************************/
/*! exports provided: Owl */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Owl\", function() { return Owl; });\n/* harmony import */ var _functions_fly__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../functions/fly */ \"./src/functions/fly.js\");\n/* harmony import */ var _functions_deliverMail__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../functions/deliverMail */ \"./src/functions/deliverMail.js\");\n/* harmony import */ var _functions_layEggs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../functions/layEggs */ \"./src/functions/layEggs.js\");\n/* harmony import */ var _functions_hunt__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../functions/hunt */ \"./src/functions/hunt.js\");\n\r\n\r\n\r\n\r\n\r\nclass Owl{\r\n    constructor(){\r\n        console.log('Owl can:');\r\n        Object(_functions_fly__WEBPACK_IMPORTED_MODULE_0__[\"fly\"])();\r\n        Object(_functions_deliverMail__WEBPACK_IMPORTED_MODULE_1__[\"deliverMail\"])();\r\n        Object(_functions_layEggs__WEBPACK_IMPORTED_MODULE_2__[\"layEggs\"])();\r\n        Object(_functions_hunt__WEBPACK_IMPORTED_MODULE_3__[\"hunt\"])();\r\n    }\r\n}\n\n//# sourceURL=webpack:///./src/classes/Owl.js?");

/***/ }),

/***/ "./src/functions/deliverMail.js":
/*!**************************************!*\
  !*** ./src/functions/deliverMail.js ***!
  \**************************************/
/*! exports provided: deliverMail */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"deliverMail\", function() { return deliverMail; });\nfunction deliverMail(){\r\n    console.log('deliverMail');\r\n}\n\n//# sourceURL=webpack:///./src/functions/deliverMail.js?");

/***/ }),

/***/ "./src/functions/eat.js":
/*!******************************!*\
  !*** ./src/functions/eat.js ***!
  \******************************/
/*! exports provided: eat */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"eat\", function() { return eat; });\nfunction eat(){\r\n    console.log('eat');\r\n}\n\n//# sourceURL=webpack:///./src/functions/eat.js?");

/***/ }),

/***/ "./src/functions/fly.js":
/*!******************************!*\
  !*** ./src/functions/fly.js ***!
  \******************************/
/*! exports provided: fly */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"fly\", function() { return fly; });\nfunction fly(){\r\n    console.log('fly');\r\n}\n\n//# sourceURL=webpack:///./src/functions/fly.js?");

/***/ }),

/***/ "./src/functions/hunt.js":
/*!*******************************!*\
  !*** ./src/functions/hunt.js ***!
  \*******************************/
/*! exports provided: hunt */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"hunt\", function() { return hunt; });\nfunction hunt(){\r\n    console.log('hunt');\r\n}\n\n//# sourceURL=webpack:///./src/functions/hunt.js?");

/***/ }),

/***/ "./src/functions/layEggs.js":
/*!**********************************!*\
  !*** ./src/functions/layEggs.js ***!
  \**********************************/
/*! exports provided: layEggs */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"layEggs\", function() { return layEggs; });\nfunction layEggs(){\r\n    console.log('layEggs');\r\n}\n\n//# sourceURL=webpack:///./src/functions/layEggs.js?");

/***/ }),

/***/ "./src/functions/run.js":
/*!******************************!*\
  !*** ./src/functions/run.js ***!
  \******************************/
/*! exports provided: run */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"run\", function() { return run; });\nfunction run(){\r\n    console.log('run');\r\n}\n\n//# sourceURL=webpack:///./src/functions/run.js?");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _classes_Han__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./classes/Han */ \"./src/classes/Han.js\");\n/* harmony import */ var _classes_Owl__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./classes/Owl */ \"./src/classes/Owl.js\");\n/*\r\n\r\n  Задание:\r\n    Написать конструктор обьекта. Отдельные функции разбить на модули\r\n    и использовать внутри самого конструктора.\r\n    Каждую функцию выделить в отдельный модуль и собрать.\r\n\r\n    Тематика - птицы.\r\n    Птицы могут:\r\n      - Нестись\r\n      - Летать\r\n      - Плавать\r\n      - Кушать\r\n      - Охотиться\r\n      - Петь\r\n      - Переносить почту\r\n      - Бегать\r\n\r\n  Составить птиц (пару на выбор, не обязательно всех):\r\n      Страус\r\n      Голубь\r\n      Курица\r\n      Пингвин\r\n      Чайка\r\n      Ястреб\r\n      Сова\r\n\r\n */\r\n\r\n\r\n\r\n\r\nconst han = new _classes_Han__WEBPACK_IMPORTED_MODULE_0__[\"Han\"](); \r\nconst owl = new _classes_Owl__WEBPACK_IMPORTED_MODULE_1__[\"Owl\"]();\r\n\n\n//# sourceURL=webpack:///./src/index.js?");

/***/ })

/******/ });