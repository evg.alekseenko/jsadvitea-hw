import {fly} from '../functions/fly'
import {deliverMail} from '../functions/deliverMail'
import {layEggs} from '../functions/layEggs'
import {hunt} from '../functions/hunt'

export class Owl{
    constructor(){
        console.log('Owl can:');
        fly();
        deliverMail();
        layEggs();
        hunt();
    }
}