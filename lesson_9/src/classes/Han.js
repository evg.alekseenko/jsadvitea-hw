import {layEggs} from '../functions/layEggs';
import {eat} from '../functions/eat';
import {run} from '../functions/run'
export class Han{
    constructor(){
        console.log('Han can:');
        layEggs();
        eat();
        run();
    }
}