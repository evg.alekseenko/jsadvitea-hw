
/*
  Задание:
  Написать скрипт который:
    1. Собирает данные с формы (3 разных полей), конвертирует их в json и выводит в консоль.
  ->  2. Сделать отдельный инпут который выполняет JSON.parse(); на ту строку что вы туда ввели и выводит результат в консоль.

  Array.from(HTMLNodeColection); -> Arary



  <form>
    <input name="name" />
    <input name="age"/>
    <input name="password"/>

    <button></button>
  </form>


    <input />

  -> '{"name" : !"23123", "age": 15, "password": "*****" }'


*/

let form = document.forms[0];
let result = document.getElementById('result');
let parse = document.getElementById('parse');
form.addEventListener('submit', (e) => {
  e.preventDefault();
  let obj = {
    name: e.target.name.value,
    age: e.target.age.value,
    password: e.target.password.value
  };
  result.value = JSON.stringify(obj);

});

parse.addEventListener('click', (e)=>{
  console.log(JSON.parse(result.value));
});