/*
  Задача:

  1. При помощи fetch получить данные:
     http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2

  2. Полученый ответ преобразовать в json вызвав метод .json с объекта ответа
  3. Выбрать случайного человека и передать в следующий чейн промиса
  4. Сделать дополнительный запрос на получение списка друзей человека
     http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2
     И дальше передать обьект:
      {
        name: userName,
        ...
        friends: friendsData
      }

     Подсказка нужно вызвать дополнительный fecth из текущего чейна.
     Для того что бы передать результат выполнения доп. запроса
     в наш первый промис используйте констуркцию:

      .then(
        response1 => {
          return fetch(...)
            .then(
              response2 => {
                ...
                формируете обьект в котором будут данные человека с
                первого запроса, например его name response1.name
                и друзья которые пришли из доп. запроса
              }
            )
        }
      )

  5. Вывести результат на страничку

  + Бонус. Для конвертации обьекта response в json использовать одну
    функцию.

*/
const fetchres = document.getElementById('fetchres');
  
function getRandom(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

  const fetchHeaders = new Headers();
  fetchHeaders.append("Content-Type", "text/plain");
  const urlPersons = 'http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2';
  const urlFriends = 'http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2';
  const options = {
    method: 'POST',
    headers: fetchHeaders
  }

  const ConvertToJSON = ( data ) => data.json();

  fetch(urlPersons, options)
    .then(ConvertToJSON)
    .then(resp1 => resp1[getRandom(0, resp1.length)] )
    .then(resp2 => { 
      return fetch(urlFriends, options)
        .then(ConvertToJSON)
        .then(resp3 => { 
          return { 
            name: resp2.name, 
            friends: resp3[0].friends
          }  
        })
      })
    .then(result => { 
      fetchres.innerHTML = `
      Fetch result, FRIENDS:
      ${JSON.stringify(result)}
      ` })


    // fetch(urlPersons, options)
    // .then(testFunc)
    // .then(test2Func)
    // .then( res => {
    //    fetch()

    // })
    // .then( func)
