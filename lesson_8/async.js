/*

  Задание:

    Написать при помощи async-await скрипт, который:

    Получает список компаний:  http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
    Перебирает, выводит табличку:

    # | Company  | Balance | Показать дату регистрации | Показать адресс |
    1.| CompName | 2000$   | button                    | button
    2.| CompName | 2000$   | 20/10/2019                | button
    3.| CompName | 2000$   | button                    | button
    4.| CompName | 2000$   | button                    | button

    Данные о дате регистрации и адресс скрывать при выводе и показывать при клике.

*/

async function getCompanyRender(place){
  const getCompanyResponse = await fetch('http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2');
  const company = await getCompanyResponse.json();
  const companyRender = await company.map((item, index)=>{
    return `<tr>
      <td>${index}</td>
      <td>${item.company}</td>
      <td>${item.balance}</td>
      <td>${item.registered}</td>
      <td>${item.address}</td>
    </tr>`;
  });
  let render = `<table>
  <th>
    <td>#</td>
    <td>Company</td>
    <td>Balance</td>
    <td>Показать дату регистрации</td>
    <td>Показать адрес</td>
  </th>
    ${companyRender}  
  </table>`;
  place.innerHTML = render;
}


const asyncResult = document.getElementById('async-result');
getCompanyRender(asyncResult);

