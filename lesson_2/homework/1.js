
  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */


var tabs = document.querySelectorAll('.tab');
// Не уверен можно ли в функциях использовать внешнюю переменную tabs
// или стоит писать pure function и в каждой функции выбирать document.querySelectorAll('.tab')

document.querySelectorAll('.showButton').forEach(function(elem) {
    elem.onclick = function(e){
      hideAllTabs();
      tabs.forEach(function(tab){
        if(tab.dataset.tab == e.target.dataset.tab){
          tab.classList.add('active');
        }
      });
    };
  });

function hideAllTabs(){
  tabs.forEach(function(elem){
    elem.classList.remove('active');
  });
}


  
