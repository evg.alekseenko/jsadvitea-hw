/*

  Задание написать простой слайдер.

    Есть массив с картинками из которых должен состоять наш слайдер.
    По умолчанию выводим первый элмемнт нашего слайдера в блок с id='slider'
    ( window.onload = func(){...} // window.addEventListener('load', function(){...}) )
    По клику на PrevSlide/NextSlide показываем предыдущий или следующий сладй соответствено.

    Для этого необходимо написать 4 функции которые будут:

    1. Срабатывать на событие load обьекта window и загружать наш слайд по умолчанию.
    2. RenderImage -> Очищать текущий контент блока #slider. Создавать нашу картинку и через метод аппенд чайлд вставлять её в слайдер.
    3. NextSlide -> По клику на NextSilde передавать currentPosition текущего слайда + 1 в функцию рендера
    4. PrevSlide -> Тоже самое что предыдущий вариант, но на кнопку PrevSlide и передавать currentPosition - 1
      + бонус сделать так что бы при достижении последнего и первого слада вас после кидало на первый и последний слайд соответственно.
      + бонсу анимация появления слайдов через навешивание дополнительных стилей

*/

var OurSliderImages = ['images/cat1.jpg', 'images/cat2.jpg', 'images/cat3.jpg', 'images/cat4.jpg', 'images/cat5.jpg', 'images/cat6.jpg', 'images/cat7.jpg', 'images/cat8.jpg'];
var currentPosition = 0;
var slider = document.getElementById('slider');


window.addEventListener('load', function(event) {
    var numbers = this.document.getElementById('numbers');
    for(var i=0;i<OurSliderImages.length;i++) {
      var dot = this.document.createElement('span');
      dot.innerText=i+1;
      dot.addEventListener('click', function(e){
        currentPosition = Number(e.target.innerText)-1;
        console.log(i);
        RenderImage();
      });
      numbers.appendChild(dot);
    }

    RenderImage();
});

Array.from(document.getElementById('SliderControls').getElementsByTagName('button')).forEach(function(elem){
  elem.addEventListener('click', function(e){
    currentPosition = (e.target.id==='PrevSilde') ?
      (currentPosition==0) ? OurSliderImages.length-1 : currentPosition-1 :  
      (currentPosition==OurSliderImages.length-1) ? 0 : currentPosition+1 ;
    RenderImage();
  });
});



function RenderImage(){  
  slider.innerHTML='';
  var img = new Image();
  img.src = OurSliderImages[currentPosition];
  img.addEventListener('load', function(e){
      e.target.classList.add('select');
  })
  slider.appendChild(img);

  Array.from(document.getElementById('numbers').getElementsByTagName('span')).forEach(function(elem){
      elem.className='';
      if(Number(elem.innerText)===currentPosition+1) {
        elem.className='select';
      }
  });
};





